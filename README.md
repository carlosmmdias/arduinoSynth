
#Arduino Synth
Arduino synth that plays "on the run" by Pink Floyd
It uses the <a href="https://github.com/sensorium/Mozzi">mozzi</a> library.
Connection instructions commented on the code.